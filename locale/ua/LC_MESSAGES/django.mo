��    0      �  C         (     )     1     Q     _  *   n      �  6   �     �               (     ;  "   X     {     �     �     �     �     �     �          $     *     ;     S  (   e  *   �     �     �  #   �  *   �     *     1     9     >     A     ]     c     k     p     }  
   �     �     �     �     �     �  V  �     =	  D   D	     �	     �	  W   �	  /   
  M   I
     �
     �
     �
     �
  2     C   >  2   �     �  %   �  #   �             9   @     z  !   �     �  1   �       J     T   i  9   �     �  A     f   G     �     �     �     �  5   �               .  3   ?  -   s  4   �     �     �     �  K   �  +   K                  $          &              
                          !   .      /                "          (   ,                  *   %                             -            '       #   	      +             )             0              Actions Add learning outcome for module Administrator Administrators All learning outcomes for areas were used. An error occured while deleting. Are you sure you want to delete the following objects? Area of education Area of knowledge Areas of education Areas of knowledge Assigning areas of education Assigning areas/fields/disciplines Choosing category of education Choosing module Choosing speciality Delete selected Description Discipline of science Discipline of science or art Disciplines of science Email Field of science Field of science or art Fields of science Learning outcome for module %s was saved Learning outcome for module %s was updated Learning outcomes for areas Level List of learning outcomes is empty. List of unused learning outcomes for areas Module Modules Next No No object has been deleted. Print Profile Save Save and add Seleced objects were deleted. Speciality Symbol Type User Year of didactic offer approval Yes, I am sure Project-Id-Version: syjon
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-30 19:40+0200
PO-Revision-Date: 2013-04-11 22:53+0100
Last-Translator: Piotr Wierzgała <piotr.wierzgala@umcs.lublin.pl>
Language-Team: syjon <syjon@umcs.lublin.pl>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Дії Додати результат в напрямку навчання Адміністратор Адміністратори Всі результати в областях навчання використано Помилка під час видалення Ви впевнені, що хочете видалити ці об'єкти? Область навчання Область знань Області навчання Області знань Приписати області навчання Приписати області/галузі/дисципліни Вибрати категорію навчання Вибір модуля Вибір спеціальності Видалити позначене Опис Дисципліна науки Дисципліна науки або мистецтва Дисципліни науки Електронна адреса Галузь науки Галузь науки або мистецтва Галузі науки Модульний результат навчання %s записано Модульний результат навчання %s актуалізовано Результати в областях навчання Рівень Список результатів навчання пустий Список невикористаних результатів в областях навчання  Модуль Модулі Далі Ні Жоден об'єкт не було видалено Друкувати Профіль Записати Записати і додати наступний Вибрані об'єкти видалено СпеціальністьСпеціальність Символ Тип Користувач Рік затвердження дидактичної пропозиції Так, впевнений/впевнена 