# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _

from apps.trinity.forms import EducationAreaForm, KnowledgeAreaForm, EducationFieldForm, EducationDisciplineForm
from apps.trinity.models import EducationArea, KnowledgeArea, EducationField, EducationDiscipline
from apps.merovingian.models import Course

from apps.trinity.views.trinity import is_learning_outcomes_administrator

TEMPLATE_ROOT = 'trinity/ea/'


@login_required    
def assign(request, course_id):
    """
    Przypisywanie obszarów kształcenia do kierunków I i II stopnia.
    """ 
    course = get_object_or_404(Course, pk=course_id)
    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.merovingian.views.course.show', kwargs={'course_id': course.id}))          
    
    # Pobranie obszarów kształcenia do formularza wielokrotnego wyboru
    education_areas = tuple((ae.id, ae.name) for ae in EducationArea.objects.all())

    if request.method == 'POST':
        form = EducationAreaForm(request.POST, education_areas=education_areas)
        if form.is_valid():
            course.educationarea_set = form.cleaned_data['education_areas']
            messages.success(request, _(u'Changes have been successfully saved'))
            return redirect(reverse('apps.trinity.views.ea.assign', kwargs={'course_id': course.id}))
    else:
        education_areas_initial = course.educationarea_set.values_list('id', flat=True)
        form = EducationAreaForm(education_areas=education_areas, initial={'education_areas': education_areas_initial})
    
    kwargs = {'form': form, 'education_areas': education_areas, 'course': course}
    return render_to_response(TEMPLATE_ROOT + 'assign.html', kwargs, context_instance=RequestContext(request))


@login_required
def assign_phd(request, course_id):
    """
    Przypisywanie obszarów, dziedzin i dyscyplin kształcenia do kierunków III stopnia
    """

    course = get_object_or_404(Course, pk=course_id)
    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.merovingian.views.course.show', kwargs={'course_id': course.id})) 
    
    knowledge_areas = tuple((ka.id, ka.name) for ka in KnowledgeArea.objects.all())
    education_fields = tuple((ef.id, ef.name) for ef in EducationField.objects.all())
    education_disciplines = tuple((ed.id, ed.name) for ed in EducationDiscipline.objects.all())
    
    if request.method == 'POST':
        knowledge_area_form = KnowledgeAreaForm(request.POST, knowledge_areas=knowledge_areas)
        education_field_form = EducationFieldForm(request.POST, education_fields=education_fields)
        education_discipline_form = EducationDisciplineForm(request.POST, education_disciplines=education_disciplines)
        
        if knowledge_area_form.is_valid() and education_field_form.is_valid() and education_discipline_form.is_valid():
            course.knowledgearea_set = knowledge_area_form.cleaned_data['knowledge_areas']
            course.educationfield_set = education_field_form.cleaned_data['education_fields']
            course.educationdiscipline_set = education_discipline_form.cleaned_data['education_disciplines']
            messages.success(request, _(u'Changes have been successfully saved'))
            return redirect(reverse('apps.trinity.views.ea.assign_phd', kwargs={'course_id': course.id}))
    else:
        knowledge_areas_initial = course.knowledgearea_set.values_list('id', flat=True)
        knowledge_area_form = KnowledgeAreaForm(knowledge_areas=knowledge_areas, initial={'knowledge_areas': knowledge_areas_initial})
        
        education_fields_initial = course.educationfield_set.values_list('id', flat=True)
        education_field_form = EducationFieldForm(education_fields=education_fields, initial={'education_fields': education_fields_initial})
        
        education_disciplines_initial = course.educationdiscipline_set.values_list('id', flat=True)
        education_discipline_form = EducationDisciplineForm(education_disciplines=education_disciplines, initial={'education_disciplines': education_disciplines_initial})
        
    context = {
        'knowledge_area_form': knowledge_area_form,
        'education_field_form': education_field_form,
        'education_discipline_form': education_discipline_form,
        'course': course
    }
    return render_to_response(TEMPLATE_ROOT + 'assign_phd.html', context, context_instance=RequestContext(request))
