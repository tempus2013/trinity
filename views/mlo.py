# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _

from apps.trinity.forms import ModuleSelect, XEEAddForm, SGroupSelect

from apps.merovingian.models import Module, SGroup, Course
from apps.trinity.models import ModuleLearningOutcome, CourseLearningOutcome

from apps.trinity.views.trinity import get_deleted_objects, is_learning_outcomes_administrator
from apps.whiterabbit.views import get_pdf_response

TEMPLATE_ROOT = 'trinity/mlo/'


def select_sgroup(request, course_id):
    course = get_object_or_404(Course, pk=course_id)
    
    form = SGroupSelect(request.POST or None, queryset=SGroup.objects.active().filter(course=course))
    if request.method == 'POST':
        if form.is_valid():
            sgroup = form.cleaned_data['sgroup']
            return redirect(reverse('apps.trinity.views.mlo.select_module', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id}))

    kwargs = {'form': form, 'course': course}
    return render_to_response(TEMPLATE_ROOT + 'select_sgroup.html', kwargs, context_instance=RequestContext(request))


def select_module(request, course_id, sgroup_id):
    """
    Wybieranie parametrów filtrowania dla modułowych efektów kształcenia.
    """
    course = get_object_or_404(Course, pk=course_id)
    sgroup = get_object_or_404(SGroup, pk=sgroup_id)
    
    form = ModuleSelect(request.POST or None, queryset=Module.objects.active().filter(sgroup=sgroup))
    if request.method == 'POST':
        if form.is_valid():
            module = form.cleaned_data['module']
            return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))

    kwargs = {'form': form, 'course': course, 'sgroup': sgroup}
    return render_to_response(TEMPLATE_ROOT+'select_module.html', kwargs, context_instance=RequestContext(request))


def show(request, course_id, sgroup_id, module_id):
    course = get_object_or_404(Course, pk=course_id)
    sgroup = get_object_or_404(SGroup, pk=sgroup_id)
    module = get_object_or_404(Module, pk=module_id)
    
    is_course_admin = is_learning_outcomes_administrator(request.user, course)
    
    mlos = ModuleLearningOutcome.objects.filter(module=module)

    kwargs = {'mlos': mlos, 'course': course, 'sgroup': sgroup, 'module': module, 'is_course_admin': is_course_admin}
    return render_to_response(TEMPLATE_ROOT+'show.html', kwargs, context_instance=RequestContext(request))


@login_required
def add(request, course_id, sgroup_id, module_id):
    course = get_object_or_404(Course, pk=course_id)
    sgroup = get_object_or_404(SGroup, pk=sgroup_id)
    module = get_object_or_404(Module, pk=module_id)
    
    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
    
    # Pole MultipleChoiceFiled musi otrzymać jako parametr choice listę par (klucz, wartość).
    course_learning_outcomes = tuple((clo.id, clo) for clo in CourseLearningOutcome.objects.filter(course=course))
    form = XEEAddForm(request.POST or None, learning_outcomes=course_learning_outcomes)
    
    if request.method == 'POST':
        if form.is_valid():
            mlo = ModuleLearningOutcome()
            mlo.symbol = form.cleaned_data['symbol']
            mlo.description = form.cleaned_data['description']
            mlo.module = module
            mlo.save()
            
            mlo.clos = form.cleaned_data['learning_outcomes']
            
            messages.success(request, _(u'Learning outcome for module %s was saved') % unicode(mlo.symbol).encode('utf-8'))
            
            if request.POST.get('submit', '') == 'save_and_add':
                return redirect(reverse('apps.trinity.views.mlo.add', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
            else:
                return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
    
    kwargs = {'form': form, 'course': course, 'sgroup': sgroup, 'module': module}
    return render_to_response(TEMPLATE_ROOT+'add_update.html', kwargs, context_instance=RequestContext(request))


@login_required
def update(request, course_id, sgroup_id, module_id, mlo_id):
    course = get_object_or_404(Course, pk=course_id)
    sgroup = get_object_or_404(SGroup, pk=sgroup_id)
    module = get_object_or_404(Module, pk=module_id)
    
    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
    
    mlo = ModuleLearningOutcome.objects.get(id=mlo_id)
    
    # Pole MultipleChoiceField musi otrzymać jako parametr choice listę par (klucz, wartość).
    course_learning_outcomes = tuple((clo.id, clo) for clo in CourseLearningOutcome.objects.filter(course=course))
    
    if request.method == 'POST':
        form = XEEAddForm(request.POST, learning_outcomes=course_learning_outcomes)
        if form.is_valid():
            mlo.symbol = form.cleaned_data['symbol']
            mlo.description = form.cleaned_data['description']
            mlo.module = module
            mlo.mees = form.cleaned_data['learning_outcomes']
            mlo.save()
            
            mlo.clos = form.cleaned_data['learning_outcomes']
            
            messages.success(request, _(u'Learning outcome for module %s was updated') % unicode(mlo.symbol).encode('utf-8'))
            return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
    else:
        form = XEEAddForm(learning_outcomes=course_learning_outcomes, initial={'symbol': mlo.symbol, 'description': mlo.description, 'learning_outcomes': mlo.clos.values_list('id', flat=True)})
    
    kwargs = {'form': form, 'course': course, 'sgroup': sgroup, 'module': module, 'mlo': mlo}
    return render_to_response(TEMPLATE_ROOT+'add_update.html', kwargs, context_instance=RequestContext(request))


@login_required
def delete(request, course_id, sgroup_id, module_id):
    course = get_object_or_404(Course, pk=course_id)
    sgroup = get_object_or_404(SGroup, pk=sgroup_id)
    module = get_object_or_404(Module, pk=module_id)
    
    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
    
    try:
        if request.method == 'POST':
            if request.POST.get('post') == 'yes':  # Użytkownik potwierdził usunięcie obiektów
                mlo_ids = request.session['mlo_ids']
                mlos = ModuleLearningOutcome.objects.filter(id__in=mlo_ids)
                mlos.delete()
                messages.success(request, _(u'Seleced objects were deleted.'))
            elif request.POST.get('post') == 'no':  # Użytkownik odrzucił usunięcie obiektów
                messages.info(request, _(u'No object has been deleted.'))
                return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
            else:  # Należy zapytać użytkownika czy na pewno chce usunąć wybrane obiekty
                mlo_ids = request.POST.getlist('selected_id')
                if mlo_ids:  # Użytkownik wybrał co najmniej jeden obiekt
                    request.session['mlo_ids'] = mlo_ids
                    mlos = ModuleLearningOutcome.objects.filter(id__in=mlo_ids)
                    objects = []               
                    objects.append(get_deleted_objects(mlos))
                    kwargs = {'objects': objects}
                    return render_to_response('trinity/delete_confirmation.html', kwargs, context_instance=RequestContext(request))
                else:  # Użytkownik nie wyrał ani jednego obiektu
                    messages.info(request, _(u'No object has been deleted.'))
                    return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))
    except:
        messages.error(request, _(u'An error occured while deleting.'))

    return redirect(reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))


def print_pdf(request, course_id, sgroup_id, module_id):
    course = get_object_or_404(Course, pk=course_id)
    sgroup = get_object_or_404(SGroup, pk=sgroup_id)
    module = get_object_or_404(Module, pk=module_id)
    
    mlos = ModuleLearningOutcome.objects.filter(module=module)
    template_context = {'course': course, 'sgroup': sgroup, 'module': module, 'mlos': mlos}
    return get_pdf_response(request, template_context, TEMPLATE_ROOT+'print.html', 'output', reverse('apps.trinity.views.mlo.show', kwargs={'course_id': course.id, 'sgroup_id': sgroup.id, 'module_id': module.id}))