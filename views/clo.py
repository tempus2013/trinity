# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse

from apps.trinity.forms import EducationCategoryForm, MEEAddForm
from apps.trinity.models import AreaLearningOutcome, CourseLearningOutcome, TrinityProfile, EducationCategory, EducationArea
from apps.merovingian.models import Module, Course

from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _

from apps.trinity.views.trinity import get_deleted_objects
from apps.whiterabbit.views import get_pdf_response

from apps.trinity.views.trinity import is_learning_outcomes_administrator

TEMPLATE_ROOT = 'trinity/clo/'


def select_category(request, course_id):
    """
    Wybieranie parametrów filtrowania dla kierunkowych efektów kształcenia.
    """
    course = get_object_or_404(Course, pk=course_id)
    form = EducationCategoryForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            education_category = form.cleaned_data['education_category']
            return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))

    kwargs = {'form': form, 'course': course}
    return render_to_response(TEMPLATE_ROOT + 'select_category.html', kwargs, context_instance=RequestContext(request))


def show(request, course_id, education_category_id):
    """
    Wyświetlanie kierunkowych efektów kształcenia.
    """
    course = get_object_or_404(Course, pk=course_id)
    education_category = get_object_or_404(EducationCategory, pk=education_category_id)
    clos = CourseLearningOutcome.objects.filter(course=course, education_category=education_category)
    
    is_course_admin = is_learning_outcomes_administrator(request.user, course)
    
    kwargs = {'clos': clos, 'course': course, 'education_category': education_category, 'is_course_admin': is_course_admin}
    
    if course.is_level_ba() or course.is_level_msc() or course.is_level_u_msc() or course.is_level_eng():  # Obszarowe efekty ksztalcenia określa się dla studiów 1 i 2 stopnia oraz jednolitych magisterskich i inżynieryjnych.
        alos = get_alos_for_course(course, education_category)
        remaining_alos = get_reamining_alos(clos, alos)
        kwargs['remaining_alos'] = remaining_alos    
   
    return render_to_response(TEMPLATE_ROOT + 'show.html', kwargs, context_instance=RequestContext(request))


@login_required
def add(request, course_id, education_category_id):
    """
    Dodawanie kierunkowego efektu kształcenia.
    """  
    course = get_object_or_404(Course, pk=course_id)
    education_category = get_object_or_404(EducationCategory, pk=education_category_id)

    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))     
    
    # Pole MultipleChoiceFiled musi otrzymać jako parametr choice listę par (klucz, wartość).
    area_learning_outcomes = tuple((alo.id, alo) for alo in get_alos_for_course(course, education_category))
    form = MEEAddForm(request.POST or None, learning_outcomes=area_learning_outcomes)
    
    if request.method == 'POST':
        if form.is_valid(): 
            clo = CourseLearningOutcome()
            clo.symbol = form.cleaned_data['symbol']
            clo.description = form.cleaned_data['description']
            clo.course = course
            clo.education_category = education_category
            clo.save()
            
            # Referencje do obszarowych efektów kształcenia można dodać dopiero po utworzeniu kierunkowego efektu kształcenia
            clo.alos = form.cleaned_data['learning_outcomes']
            
            messages.success(request, _(u'Course learning outcome %s has been saved') % unicode(clo.symbol).encode('utf-8'))

            if request.POST['submit'] == 'save':
                return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))
            else:
                return redirect(reverse('apps.trinity.views.clo.add', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))
    
    kwargs = {'form': form, 'course': course, 'education_category': education_category}
    return render_to_response(TEMPLATE_ROOT + 'add_update.html', kwargs, context_instance=RequestContext(request))


@login_required
def update(request, course_id, education_category_id, clo_id):
    """
    Aktualizacja kierunkowego efektu kształcenia o zadanym id.
    """
    course = get_object_or_404(Course, pk=course_id)
    education_category = get_object_or_404(EducationCategory, pk=education_category_id)
    
    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id})) 
    
    clo = CourseLearningOutcome.objects.get(id=clo_id)
    
    # Pole MultipleChoiceFiled musi otrzymać jako parametr choice listę par (klucz, wartość).
    area_learning_outcomes = tuple((alo.id, alo) for alo in get_alos_for_course(course, education_category))
    
    if request.method == 'POST':
        form = MEEAddForm(request.POST, learning_outcomes=area_learning_outcomes)
        if form.is_valid():
            clo.symbol = form.cleaned_data['symbol']
            clo.description = form.cleaned_data['description']
            clo.course = course
            clo.alos = form.cleaned_data['learning_outcomes']
            clo.education_category = education_category
            clo.save()
            
            messages.success(request, _(u'Course learning outcome %s was updated') % unicode(clo.symbol).encode('utf-8'))
            return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))
    else:
        initial = {'symbol': clo.symbol, 'description': clo.description, 'learning_outcomes': clo.alos.values_list('id', flat=True)}
        form = MEEAddForm(learning_outcomes=area_learning_outcomes, initial=initial)
    
    kwargs = {'form': form, 'course': course, 'education_category': education_category, 'clo': clo}
    return render_to_response(TEMPLATE_ROOT + 'add_update.html', kwargs, context_instance=RequestContext(request))   


@login_required
def delete(request, course_id, education_category_id):
    """
    Usuwa kierunkowy efekt kształcenia o zadanym id.
    """
    course = get_object_or_404(Course, pk=course_id)
    education_category = get_object_or_404(EducationCategory, pk=education_category_id)
    
    if not is_learning_outcomes_administrator(request.user, course):
        messages.error(request, _("You do not have permission to execute selected operation"))
        return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id})) 
    
    try:
        if request.method == 'POST':
            if request.POST.get('post') == 'yes':  # Użytkownik potwierdził usunięcie obiektów
                clo_ids = request.session['clo_ids']
                clos = CourseLearningOutcome.objects.filter(id__in=clo_ids)
                clos.delete()
                messages.success(request, _(u'Seleced objects were deleted.'))
            elif request.POST.get('post') == 'no':  # Użytkownik odrzucił usunięcie obiektów
                messages.info(request, _(u'No object has been deleted.'))
                return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))
            else: # Należy zapytać użytkownika czy na pewno chce usunąć wybrane obiekty
                clo_ids = request.POST.getlist('selected_id')
                if clo_ids:  # Użytkownik wybrał co najmniej jeden obiekt
                    request.session['clo_ids'] = clo_ids
                    clos = CourseLearningOutcome.objects.filter(id__in=clo_ids)
                    objects = []               
                    objects.append(get_deleted_objects(clos))
                    kwargs = {'objects': objects}
                    return render_to_response('trinity/delete_confirmation.html', kwargs, context_instance=RequestContext(request))
                else: # Użytkownik nie wyrał ani jednego obiektu
                    messages.info(request, _(u'No object has been deleted.'))
                    return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))
    except:
        messages.error(request, _(u'An error occured while deleting.'))

    return redirect(reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))


def print_pdf(request, course_id, education_category_id):
    course = get_object_or_404(Course, pk=course_id)
    education_category = get_object_or_404(EducationCategory, pk=education_category_id)
    clos = CourseLearningOutcome.objects.filter(course=course)
    template_context = {'course': course, 'clos': clos}
    return get_pdf_response(request, template_context, TEMPLATE_ROOT+'print.html', 'output', reverse('apps.trinity.views.clo.show', kwargs={'course_id': course.id, 'education_category_id': education_category.id}))


"""
-----------------------------------------------------------------
--- FUNKCJE POMOCNICZNE
-----------------------------------------------------------------
"""


def get_reamining_alos(clos, alos):
    """
    Zwraca zbiór obszarowych efektów kształcenia, które nie zostały powiązane z kierunkowymi efektami kształcenia przekazanymi jako parametr.
    Lista obszarowych efektów kształcenia, które powinny zostać przypisane jest przekazana jako parametr.
    @param clos: Lista kierunkowych efektów kształcenia
    @param alos: Lista obszarowych efektów kształcenia
    """
    clos_alos_set = set()
    for clo in clos:
        for alo in clo.alos.all():
            clos_alos_set.add(alo)
    
    alos_set = set()
    for alo in alos:
        alos_set.add(alo)
        
    return alos_set - clos_alos_set


def get_alos_for_course(course, education_category):
    """
    Pobiera obszarowe efekty kształcenia dla przekazanej w parametrach konfiguracji.
    Dla studiów magisterskich i inżynieryjnych pobierane są obszarowe efekty ksztalcenia z poziomu I i II.
    @param course: Kierunek
    @param education_category: Kategoria kształcenia np. wiedza, umiejętności.   
    """
    if course.is_level_u_msc() or course.is_level_eng():  # Poziom kierunku to magisterskie pięcioletnie lub inżynieryjne
        aees = AreaLearningOutcome.objects.filter(
            education_area__in=course.educationarea_set.all(),
            education_category=education_category,
            education_profile=course.profile
        )
    else:
        aees = AreaLearningOutcome.objects.filter(
            education_level=course.level,
            education_area__in=course.educationarea_set.all(),
            education_category=education_category,
            education_profile=course.profile
        )
    return aees