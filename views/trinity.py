# -*- coding: utf-8 -*-

"""
-----------------------------------------------------------------
--- FUNKCJE POMOCNICZNE
-----------------------------------------------------------------
"""


def is_learning_outcomes_administrator(user, course):
    """
    Returns True if user can manage learning outcomes connected with specified speciality.
    @param user: User object
    @param course: Course object 
    """
    if user.is_superuser:
        return True
    
    is_administrator = False
    
    try:
        trinity_profile = user.get_profile().trinityprofile  # Gets user's learning outcomes administrator profile
        if course in trinity_profile.courses.all():  # Checkes if user has rights to speciality passed as a parameter
            is_administrator = True
    except:
        is_administrator = False
    
    return is_administrator

# -----------------------------------
# --- POTWIERDZENIE USUNIĘCIA
# -----------------------------------

from django.contrib.admin.util import NestedObjects
from django.utils.text import capfirst
from django.utils.encoding import force_text


def get_deleted_objects(objs):
    collector = NestedObjects(using='default')
    collector.collect(objs)

    def format_callback(obj):
        opts = obj._meta
        return '%s: %s' % (capfirst(unicode(opts.verbose_name)), force_text(obj))

    to_delete = collector.nested(format_callback)
    return to_delete

# -----------------------------------
# --- ZAPISYWANIE PLIKÓW PDF
# -----------------------------------


def mreplace(s, chararray, newchararray):
    """
    Zamienia wszystkie wystąpienia znaków w napisie s z tablicy chararray na odpowiednie wystąpienia w tablicy newchararray. 
    """
    for a, b in zip(chararray, newchararray):
        s = s.replace(a, b)
    return s


def utf2ascii(s):
    chararray = [u'ą', u'ć', u'ę', u'ł', u'ń', u'ó', u'ś', u'ź', u'ż', u'Ą', u'Ć', u'Ę', u'Ł', u'Ń', u'Ó', u'Ś', u'Ź', u'Ż']
    newchararray = [u'a', u'c', u'e', u'l', u'n', u'o', u's', u'z', u'z', u'A', u'C', u'E', u'L', u'N', u'O', u'S', u'Z', u'Z']
    return mreplace(s, chararray, newchararray)
