# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns

# Obszary kształcenia
urlpatterns = patterns('',
    (r'ea/assign/(?P<course_id>\d+)$', 'apps.trinity.views.ea.assign'),
    (r'ea/assign_phd/(?P<course_id>\d+)$', 'apps.trinity.views.ea.assign_phd'),
)

# Kierunkowe efekty kształcenia
urlpatterns += patterns('', 
    (r'clo/select-category/(?P<course_id>\d+)$', 'apps.trinity.views.clo.select_category'),
    (r'clo/show/(?P<course_id>\d+)/(?P<education_category_id>\d+)$', 'apps.trinity.views.clo.show'),
    (r'clo/add/(?P<course_id>\d+)/(?P<education_category_id>\d+)$', 'apps.trinity.views.clo.add'),
    (r'clo/update/(?P<course_id>\d+)/(?P<education_category_id>\d+)/(?P<clo_id>\d+)$', 'apps.trinity.views.clo.update'),
    (r'clo/delete/(?P<course_id>\d+)/(?P<education_category_id>\d+)$', 'apps.trinity.views.clo.delete'),
    (r'clo/print-pdf/(?P<course_id>\d+)/(?P<education_category_id>\d+)$', 'apps.trinity.views.clo.print_pdf'),
)

# Modułowe efekty kształcenia
urlpatterns += patterns('',  
    (r'mlo/select_sgroup/(?P<course_id>\d+)$', 'apps.trinity.views.mlo.select_sgroup'),
    (r'mlo/select-module/(?P<course_id>\d+)/(?P<sgroup_id>\d+)$', 'apps.trinity.views.mlo.select_module'),
    (r'mlo/show/(?P<course_id>\d+)/(?P<sgroup_id>\d+)/(?P<module_id>\d+)$', 'apps.trinity.views.mlo.show'),
    (r'mlo/add/(?P<course_id>\d+)/(?P<sgroup_id>\d+)/(?P<module_id>\d+)$', 'apps.trinity.views.mlo.add'),
    (r'mlo/update/(?P<course_id>\d+)/(?P<sgroup_id>\d+)/(?P<module_id>\d+)/(?P<mlo_id>\d+)$', 'apps.trinity.views.mlo.update'),
    (r'mlo/delete/(?P<course_id>\d+)/(?P<sgroup_id>\d+)/(?P<module_id>\d+)$', 'apps.trinity.views.mlo.delete'),
    (r'mlo/print-pdf/(?P<course_id>\d+)/(?P<sgroup_id>\d+)/(?P<module_id>\d+)$', 'apps.trinity.views.mlo.print_pdf'),
)