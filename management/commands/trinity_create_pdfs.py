# -*- coding: utf-8 -*-
'''
Created on 27-08-2012

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
import syjon
from django.conf import settings

from apps.merovingian.models import Course
from apps.trinity.models import CourseLearningOutcome, ModuleLearningOutcome
from apps.metacortex.models import SyllabusModule, SyllabusSubject, SyllabusPractice

from apps.whiterabbit.views import get_pdf_file

class Command(BaseCommand):
    help = u'Zapisuje wszystkie efekty kształcenia do pliku.'

    def handle(self, *args, **options):    
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))

        courses = Course.objects.all()
        for course in courses:
            self.create_clos_pdfs(course)
            self.create_mlos_pdfs(course)

    def create_clos_pdfs(self, course):
        clos = CourseLearningOutcome.objects.filter(course=course)
        template_name = 'trinity/clo/print.html'
        file_name = u'%s.pdf' % (unicode(course))
        template_context = {'course': course, 'clos': clos}
        get_pdf_file(template_context, template_name, file_name, path=u'/tmp/trinity/clos/')
        self.stdout.write('%s\n' % file_name)

    def create_mlos_pdfs(self, course):
        template_name = 'trinity/mlo/print.html'
        for sgroup in course.sgroup_set.all():
            for module in sgroup.modules.all():
                file_name = u'%s.pdf' % (unicode(module))
                mlos = ModuleLearningOutcome.objects.filter(module=module)
                template_context = {'course': course, 'sgroup': sgroup, 'module': module, 'mlos': mlos}
                get_pdf_file(template_context, template_name, file_name, path=u'/tmp/trinity/mlos/%s/%s' % (unicode(sgroup), unicode(course)))
                self.stdout.write('%s\n' % file_name)
