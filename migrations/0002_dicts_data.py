# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.

        # ----------------------------------------------
        # --- Education Area
        # ----------------------------------------------

        if len(orm['trinity.EducationArea'].objects.filter(name_en='agricultural')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'agricultural'
            obj.name_pl = u'rolniczy'
            obj.name_en = u'agricultural'
            obj.name_ua = u'агрокультурний'
            obj.name_ru = u'агрокультурный'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='art')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'art'
            obj.name_pl = u'sztuki'
            obj.name_en = u'art'
            obj.name_ua = u'мистецтво'
            obj.name_ru = u'искусство'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='engineering')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'engineering'
            obj.name_pl = u'inżynierski'
            obj.name_en = u'engineering'
            obj.name_ua = u'технічний'
            obj.name_ru = u'технический'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='humanistic')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'humanistic'
            obj.name_pl = u'humanistyczny'
            obj.name_en = u'humanistic'
            obj.name_ua = u'гуманітарний'
            obj.name_ru = u'гуманитарный'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='medical')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'medical'
            obj.name_pl = u'medyczny'
            obj.name_en = u'medical'
            obj.name_ua = u'медичний'
            obj.name_ru = u'медицинский'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='natural')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'natural'
            obj.name_pl = u'przyrodniczy'
            obj.name_en = u'natural'
            obj.name_ua = u'природничий'
            obj.name_ru = u'естественные науки'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='scientific')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'scientific'
            obj.name_pl = u'ścisły'
            obj.name_en = u'scientific'
            obj.name_ua = u'точні науки'
            obj.name_ru = u'точные науки'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='social')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'social'
            obj.name_pl = u'społeczny'
            obj.name_en = u'social'
            obj.name_ua = u'суспільний'
            obj.name_ru = u'общественный'
            obj.save()

        if len(orm['trinity.EducationArea'].objects.filter(name_en='technical')) == 0:
            obj = orm['trinity.EducationArea']()
            obj.name = u'technical'
            obj.name_pl = u'techniczny'
            obj.name_en = u'technical'
            obj.name_ua = u'технічний'
            obj.name_ru = u'технический'
            obj.save()

        # ----------------------------------------------
        # --- Education Category
        # ----------------------------------------------

        if len(orm['trinity.EducationCategory'].objects.filter(name_en='skills')) == 0:
            obj = orm['trinity.EducationCategory']()
            obj.name = u'skills'
            obj.name_pl = u'umiejętności'
            obj.name_en = u'skills'
            obj.name_ua = u'вміння'
            obj.name_ru = u'умения'
            obj.save()

        if len(orm['trinity.EducationCategory'].objects.filter(name_en='knowledge')) == 0:
            obj = orm['trinity.EducationCategory']()
            obj.name = u'knowledge'
            obj.name_pl = u'wiedza'
            obj.name_en = u'knowledge'
            obj.name_ua = u'знання'
            obj.name_ru = u'знания'
            obj.save()

        if len(orm['trinity.EducationCategory'].objects.filter(name_en='competences')) == 0:
            obj = orm['trinity.EducationCategory']()
            obj.name = u'competences'
            obj.name_pl = u'kompetencje społeczne'
            obj.name_en = u'competences'
            obj.name_ua = u'соціальна компетенція'
            obj.name_ru = u'социальная компетенция'
            obj.save()

        # ----------------------------------------------
        # --- Knowledge Area, Education Discipline, Education Field
        # ----------------------------------------------

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of agricultural, forestry and veterinary')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of agricultural, forestry and veterinary'
            area.name_pl = u'obszar nauk rolniczych, leśnych i weterynaryjnych'
            area.name_en = u'area of agricultural, forestry and veterinary'
            area.name_ua = u'область агронаук, лісництва і ветеринарії'
            area.name_ru = u'область агронаук, лесоводства и ветеринарии'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='veterinary')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'veterinary'
            field.name_pl = u'dziedzina nauk weterynaryjnych'
            field.name_en = u'veterinary'
            field.name_ua = u'область ветеринарних наук'
            field.name_ru = u'область ветеринарных наук'
            field.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='forestry')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'forestry'
            field.name_pl = u'dziedzina nauk leśnych'
            field.name_en = u'forestry'
            field.name_ua = u'область наук про лісництво'
            field.name_ru = u'область наук по лесоводству'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='forestry')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'forestry'
            discipline.name_pl = u'leśnictwo'
            discipline.name_en = u'forestry'
            discipline.name_ua = u'лісництво'
            discipline.name_ru = u'лесное хозяйство'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='agricultural')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'agricultural'
            field.name_pl = u'dziedzina nauk rolniczych'
            field.name_en = u'agricultural'
            field.name_ua = u'область агронаук'
            field.name_ru = u'область агронаук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='agricultural engineering')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'agricultural engineering'
            discipline.name_pl = u'inżynieria rolnicza'
            discipline.name_en = u'agricultural engineering'
            discipline.name_ua = u'агроінженерія'
            discipline.name_ru = u'агроинженерия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='agronomy')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'agronomy'
            discipline.name_pl = u'agronomia'
            discipline.name_en = u'agronomy'
            discipline.name_ua = u'агрономія'
            discipline.name_ru = u'агрономия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biotechnology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biotechnology'
            discipline.name_pl = u'biotechnologia'
            discipline.name_en = u'biotechnology'
            discipline.name_ua = u'біотехнологія'
            discipline.name_ru = u'биотехнология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='fishing')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'fishing'
            discipline.name_pl = u'rybactwo'
            discipline.name_en = u'fishing'
            discipline.name_ua = u'риболовство'
            discipline.name_ru = u'рыболовство'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='food technology and nutrition')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'food technology and nutrition'
            discipline.name_pl = u'technologia żywności i żywienia'
            discipline.name_en = u'food technology and nutrition'
            discipline.name_ua = u'харчова технологія'
            discipline.name_ru = u'пищевая технология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='gardening')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'gardening'
            discipline.name_pl = u'ogrodnictwo'
            discipline.name_en = u'gardening'
            discipline.name_ua = u'садівництво'
            discipline.name_ru = u'садоводство'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='protection and development of the environment')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'protection and development of the environment'
            discipline.name_pl = u'ochrona i kształtowanie środowiska'
            discipline.name_en = u'protection and development of the environment'
            discipline.name_ua = u'охорона і оптимізація довкілля'
            discipline.name_ru = u'охрана и оптимизация окружающей среды'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='zootechnics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'zootechnics'
            discipline.name_pl = u'zootechnika'
            discipline.name_en = u'zootechnics'
            discipline.name_ua = u'зоотехніка'
            discipline.name_ru = u'зоотехника'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of art')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of art'
            area.name_pl = u'obszar sztuki'
            area.name_en = u'area of art'
            area.name_ua = u'область мистецтва'
            area.name_ru = u'область искусства'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='theater')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'theater'
            field.name_pl = u'dziedzina sztuk teatralnych'
            field.name_en = u'theater'
            field.name_ua = u'область театрального мистецтва'
            field.name_ru = u'область театральных искусств'
            field.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='plastic arts')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'plastic arts'
            field.name_pl = u'dziedzina sztuk plastycznych'
            field.name_en = u'plastic arts'
            field.name_ua = u'область образотворих мистецтв'
            field.name_ru = u'область изобразительных искусств'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='art design')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'art design'
            discipline.name_pl = u'sztuki projektowe'
            discipline.name_en = u'art design'
            discipline.name_ua = u'художнє моделювання'
            discipline.name_ru = u'художественное моделирование'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='conservation and restoration of works of art')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'conservation and restoration of works of art'
            discipline.name_pl = u'konserwacja i restauracja dzieł sztuki'
            discipline.name_en = u'conservation and restoration of works of art'
            discipline.name_ua = u'консервація і реставрація творів мистецтва'
            discipline.name_ru = u'консервация и реставрация произведений искусства'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='fine arts')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'fine arts'
            discipline.name_pl = u'sztuki piękne'
            discipline.name_en = u'fine arts'
            discipline.name_ua = u'образотворче мистецтво'
            discipline.name_ru = u'изобразительное искусство'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='musical arts')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'musical arts'
            field.name_pl = u'dziedzina sztuk muzycznych'
            field.name_en = u'musical arts'
            field.name_ua = u'область кінематографу'
            field.name_ru = u'область кинематографа'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='composition and music theory')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'composition and music theory'
            discipline.name_pl = u'kompozycja i teoria muzyki'
            discipline.name_en = u'composition and music theory'
            discipline.name_ua = u'композиція і теорія музики'
            discipline.name_ru = u'композиция и теория музыки'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='conducting')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'conducting'
            discipline.name_pl = u'dyrygentura'
            discipline.name_en = u'conducting'
            discipline.name_ua = u'диригування'
            discipline.name_ru = u'дирижирование'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='instrumental')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'instrumental'
            discipline.name_pl = u'instrumentalistyka'
            discipline.name_en = u'instrumental'
            discipline.name_ua = u'інструменталістика'
            discipline.name_ru = u'инструменталистика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='rhythmics and dance')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'rhythmics and dance'
            discipline.name_pl = u'rytmika i taniec'
            discipline.name_en = u'rhythmics and dance'
            discipline.name_ua = u'ритміка і танець'
            discipline.name_ru = u'ритмика и танец'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='sound engineering')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'sound engineering'
            discipline.name_pl = u'reżyseria dźwięku'
            discipline.name_en = u'sound engineering'
            discipline.name_ua = u'звукорежисура'
            discipline.name_ru = u'звукорежиссура'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='vocal')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'vocal'
            discipline.name_pl = u'wokalistyka'
            discipline.name_en = u'vocal'
            discipline.name_ua = u'вокалістика'
            discipline.name_ru = u'вокалистика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='film arts')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'film arts'
            field.name_pl = u'dziedzina sztuk filmowych'
            field.name_en = u'film arts'
            field.name_ua = u'мистецтв фільм'
            field.name_ru = u'искусств фильм'
            field.save()

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of medicine and health sciences, and physical education')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of medicine and health sciences, and physical education'
            area.name_pl = u'obszar nauk medycznych i nauk o zdrowiu oraz nauk o kulturze fizycznej'
            area.name_en = u'area of medicine and health sciences, and physical education'
            area.name_ua = u'область медичних наук, наук про здоровя та наук про фізичну культуру'
            area.name_ru = u'область медицинских наук, наук о здоровье и наук по физической культуре'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='about physical education')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'about physical education'
            field.name_pl = u'dziedzina nauk o kulturze fizycznej'
            field.name_en = u'about physical education'
            field.name_ua = u'область наук про фізичну культуру'
            field.name_ru = u'область наук по физической культуре'
            field.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='pharmaceutical')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'pharmaceutical'
            field.name_pl = u'dziedzina nauk farmaceutycznych'
            field.name_en = u'pharmaceutical'
            field.name_ua = u'область фармацевтичних наук'
            field.name_ru = u'область фармацевтических наук'
            field.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='medical')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'medical'
            field.name_pl = u'dziedzina nauk medycznych'
            field.name_en = u'medical'
            field.name_ua = u'область медичних наук'
            field.name_ru = u'область медицинских наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='medical biology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'medical biology'
            discipline.name_pl = u'biologia medyczna'
            discipline.name_en = u'medical biology'
            discipline.name_ua = u'медична біологія'
            discipline.name_ru = u'медицинская биология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='medicine')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'medicine'
            discipline.name_pl = u'medycyna'
            discipline.name_en = u'medicine'
            discipline.name_ua = u'медицина'
            discipline.name_ru = u'медицина'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='stomatology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'stomatology'
            discipline.name_pl = u'stomatologia'
            discipline.name_en = u'stomatology'
            discipline.name_ua = u'стоматологія'
            discipline.name_ru = u'стоматология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of of natural sciences')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of of natural sciences'
            area.name_pl = u'obszar nauk przyrodniczych'
            area.name_en = u'area of of natural sciences'
            area.name_ua = u'область природничих наук'
            area.name_ru = u'область естественных наук'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='about Earth')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'about Earth'
            field.name_pl = u'dziedzina nauk o Ziemi'
            field.name_en = u'about Earth'
            field.name_ua = u'область наук про Землю'
            field.name_ru = u'область наук о Земле'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='geography')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'geography'
            discipline.name_pl = u'geografia'
            discipline.name_en = u'geography'
            discipline.name_ua = u'географія'
            discipline.name_ru = u'география'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='geology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'geology'
            discipline.name_pl = u'geologia'
            discipline.name_en = u'geology'
            discipline.name_ua = u'геологія'
            discipline.name_ru = u'геология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='geophysics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'geophysics'
            discipline.name_pl = u'geofizyka'
            discipline.name_en = u'geophysics'
            discipline.name_ua = u'геофізика'
            discipline.name_ru = u'геофизика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='oceanology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'oceanology'
            discipline.name_pl = u'oceanologia'
            discipline.name_en = u'oceanology'
            discipline.name_ua = u'океанологія'
            discipline.name_ru = u'океанология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='biological')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'biological'
            field.name_pl = u'dziedzina nauk biologicznych'
            field.name_en = u'biological'
            field.name_ua = u'область біологічних наук'
            field.name_ru = u'область биологических наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biochemistry')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biochemistry'
            discipline.name_pl = u'biochemia'
            discipline.name_en = u'biochemistry'
            discipline.name_ua = u'біохімія'
            discipline.name_ru = u'биохимия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biology'
            discipline.name_pl = u'biologia'
            discipline.name_en = u'biology'
            discipline.name_ua = u'біологія'
            discipline.name_ru = u'биология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biophysics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biophysics'
            discipline.name_pl = u'biofizyka'
            discipline.name_en = u'biophysics'
            discipline.name_ua = u'біофізика'
            discipline.name_ru = u'биофизика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biotechnology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biotechnology'
            discipline.name_pl = u'biotechnologia'
            discipline.name_en = u'biotechnology'
            discipline.name_ua = u'біотехнологія'
            discipline.name_ru = u'биотехнология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='ecology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'ecology'
            discipline.name_pl = u'ekologia'
            discipline.name_en = u'ecology'
            discipline.name_ua = u'екологія'
            discipline.name_ru = u'экология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='environmental protection')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'environmental protection'
            discipline.name_pl = u'ochrona środowiska'
            discipline.name_en = u'environmental protection'
            discipline.name_ua = u'охорона навколишнього середовища'
            discipline.name_ru = u'охрана окружающей среды'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='microbiology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'microbiology'
            discipline.name_pl = u'mikrobiologia'
            discipline.name_en = u'microbiology'
            discipline.name_ua = u'мікробіологія'
            discipline.name_ru = u'микробиология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of science')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of science'
            area.name_pl = u'obszar nauk ścisłych'
            area.name_en = u'area of science'
            area.name_ua = u'область точних наук'
            area.name_ru = u'область технических наук'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='chemical')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'chemical'
            field.name_pl = u'dziedzina nauk chemicznych'
            field.name_en = u'chemical'
            field.name_ua = u'область хімічних наук'
            field.name_ru = u'область химических наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biochemistry')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biochemistry'
            discipline.name_pl = u'biochemia'
            discipline.name_en = u'biochemistry'
            discipline.name_ua = u'біохімія'
            discipline.name_ru = u'биохимия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biotechnology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biotechnology'
            discipline.name_pl = u'biotechnologia'
            discipline.name_en = u'biotechnology'
            discipline.name_ua = u'біотехнологія'
            discipline.name_ru = u'биотехнология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='chemical technology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'chemical technology'
            discipline.name_pl = u'technologia chemiczna'
            discipline.name_en = u'chemical technology'
            discipline.name_ua = u'хімічна технологія'
            discipline.name_ru = u'химическая технология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='chemistry')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'chemistry'
            discipline.name_pl = u'chemia'
            discipline.name_en = u'chemistry'
            discipline.name_ua = u'хімія'
            discipline.name_ru = u'химия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='environmental protection')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'environmental protection'
            discipline.name_pl = u'ochrona środowiska'
            discipline.name_en = u'environmental protection'
            discipline.name_ua = u'охорона навколишнього середовища'
            discipline.name_ru = u'охрана окружающей среды'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='physical')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'physical'
            field.name_pl = u'dziedzina nauk fizycznych'
            field.name_en = u'physical'
            field.name_ua = u'область фізичних наук'
            field.name_ru = u'область физических наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='astronomy')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'astronomy'
            discipline.name_pl = u'astronomia'
            discipline.name_en = u'astronomy'
            discipline.name_ua = u'астрономія'
            discipline.name_ru = u'астрономия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biophysics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biophysics'
            discipline.name_pl = u'biofizyka'
            discipline.name_en = u'biophysics'
            discipline.name_ua = u'біофізика'
            discipline.name_ru = u'биофизика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='geophysics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'geophysics'
            discipline.name_pl = u'geofizyka'
            discipline.name_en = u'geophysics'
            discipline.name_ua = u'геофізика'
            discipline.name_ru = u'геофизика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='physics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'physics'
            discipline.name_pl = u'fizyka'
            discipline.name_en = u'physics'
            discipline.name_ua = u'фізика'
            discipline.name_ru = u'физика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='mathematical')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'mathematical'
            field.name_pl = u'dziedzina nauk matematycznych'
            field.name_en = u'mathematical'
            field.name_ua = u'область математичних наук'
            field.name_ru = u'область математических наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='computer science')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'computer science'
            discipline.name_pl = u'informatyka'
            discipline.name_en = u'computer science'
            discipline.name_ua = u'компютерні науки'
            discipline.name_ru = u'компьютерные науки'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='mathematics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'mathematics'
            discipline.name_pl = u'matematyka'
            discipline.name_en = u'mathematics'
            discipline.name_ua = u'математика'
            discipline.name_ru = u'математика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of technical sciences')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of technical sciences'
            area.name_pl = u'obszar nauk technicznych'
            area.name_en = u'area of technical sciences'
            area.name_ua = u'область технічних наук'
            area.name_ru = u'область искусства'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='technical')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'technical'
            field.name_pl = u'dziedzina nauk technicznych'
            field.name_en = u'technical'
            field.name_ua = u'область технічних наук'
            field.name_ru = u'область технических наукнаук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='architecture and urban planning')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'architecture and urban planning'
            discipline.name_pl = u'architektura i urbanistyka'
            discipline.name_en = u'architecture and urban planning'
            discipline.name_ua = u'архітектура і урбаністика'
            discipline.name_ru = u'архитектура и урбанистика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='automation and robotics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'automation and robotics'
            discipline.name_pl = u'automatyka i robotyka'
            discipline.name_en = u'automation and robotics'
            discipline.name_ua = u'автоматизація і робототехніка'
            discipline.name_ru = u'автоматизация и робототехника'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biocybernetics and biomedical engineering')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biocybernetics and biomedical engineering'
            discipline.name_pl = u'biocybernetyka i inżynieria biomedyczna'
            discipline.name_en = u'biocybernetics and biomedical engineering'
            discipline.name_ua = u'біокібернетика і біомедична інженерія'
            discipline.name_ru = u'биокибернетика и биомедицинская инженерия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='biotechnology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'biotechnology'
            discipline.name_pl = u'biotechnologia'
            discipline.name_en = u'biotechnology'
            discipline.name_ua = u'біотехнологія'
            discipline.name_ru = u'биотехнология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='building')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'building'
            discipline.name_pl = u'budownictwo'
            discipline.name_en = u'building'
            discipline.name_ua = u'будівництво'
            discipline.name_ru = u'строительство'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='chemical engineering')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'chemical engineering'
            discipline.name_pl = u'inżynieria chemiczna'
            discipline.name_en = u'chemical engineering'
            discipline.name_ua = u'хімічна інженерія'
            discipline.name_ru = u'химическая инженерия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='chemical technology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'chemical technology'
            discipline.name_pl = u'technologia chemiczna'
            discipline.name_en = u'chemical technology'
            discipline.name_ua = u'хімічна технологія'
            discipline.name_ru = u'химическая технология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='computer science')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'computer science'
            discipline.name_pl = u'informatyka'
            discipline.name_en = u'computer science'
            discipline.name_ua = u'компютерні науки'
            discipline.name_ru = u'компьютерные науки'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='construction and operation of machinery')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'construction and operation of machinery'
            discipline.name_pl = u'budowa i eksploatacja maszyn'
            discipline.name_en = u'construction and operation of machinery'
            discipline.name_ua = u'будівництво і експлуатація машин'
            discipline.name_ru = u'строительство и эксплуатация машин'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='electronics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'electronics'
            discipline.name_pl = u'elektronika'
            discipline.name_en = u'electronics'
            discipline.name_ua = u'електроніка'
            discipline.name_ru = u'электроника'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='electrotechnics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'electrotechnics'
            discipline.name_pl = u'elektrotechnika'
            discipline.name_en = u'electrotechnics'
            discipline.name_ua = u'електротехніка'
            discipline.name_ru = u'электротехника'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='energetics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'energetics'
            discipline.name_pl = u'energetyka'
            discipline.name_en = u'energetics'
            discipline.name_ua = u'енергетика'
            discipline.name_ru = u'энергетика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='environmental engineering')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'environmental engineering'
            discipline.name_pl = u'inżynieria środowiska'
            discipline.name_en = u'environmental engineering'
            discipline.name_ua = u'інженерія навколишнього середовища'
            discipline.name_ru = u'инженерия окружающей среды'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='geodesy and cartography')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'geodesy and cartography'
            discipline.name_pl = u'geodezja i kartografia'
            discipline.name_en = u'geodesy and cartography'
            discipline.name_ua = u'геодезія і картографія'
            discipline.name_ru = u'геодезия и картография'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='materials science')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'materials science'
            discipline.name_pl = u'inżynieria materiałowa'
            discipline.name_en = u'materials science'
            discipline.name_ua = u'інженерія матеріалів'
            discipline.name_ru = u'инженерия материалов'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='mechanics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'mechanics'
            discipline.name_pl = u'mechanika'
            discipline.name_en = u'mechanics'
            discipline.name_ua = u'механіка'
            discipline.name_ru = u'механика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='metallurgy')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'metallurgy'
            discipline.name_pl = u'metalurgia'
            discipline.name_en = u'metallurgy'
            discipline.name_ua = u'металургія'
            discipline.name_ru = u'металургия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='mining and engineering geology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'mining and engineering geology'
            discipline.name_pl = u'górnictwo i geologia inżynierska'
            discipline.name_en = u'mining and engineering geology'
            discipline.name_ua = u'гірництво і інженерна геологія'
            discipline.name_ru = u'горная промышленность и инженерная геология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='production engineering')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'production engineering'
            discipline.name_pl = u'inżynieria produkcji'
            discipline.name_en = u'production engineering'
            discipline.name_ua = u'інженерія продукції'
            discipline.name_ru = u'инженерия продукции'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='telecommunication')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'telecommunication'
            discipline.name_pl = u'telekomunikacja'
            discipline.name_en = u'telecommunication'
            discipline.name_ua = u'телекомунікація'
            discipline.name_ru = u'телекоммуникация'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='textiles')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'textiles'
            discipline.name_pl = u'włókiennictwo'
            discipline.name_en = u'textiles'
            discipline.name_ua = u'текстильна промисловість'
            discipline.name_ru = u'текстильная промышленность'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='transport')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'transport'
            discipline.name_pl = u'transport'
            discipline.name_en = u'transport'
            discipline.name_ua = u'транспорт'
            discipline.name_ru = u'транспорт'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of the humanities')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of the humanities'
            area.name_pl = u'obszar nauk humanistycznych'
            area.name_en = u'area of the humanities'
            area.name_ua = u'область гуманітарних наук'
            area.name_ru = u'область гуманитарных наук'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='theological')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'theological'
            field.name_pl = u'dziedzina nauk teologicznych'
            field.name_en = u'theological'
            field.name_ua = u'область теологічних наук'
            field.name_ru = u'область теологических наук'
            field.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='humanistic')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'humanistic'
            field.name_pl = u'dziedzina nauk humanistycznych'
            field.name_en = u'humanistic'
            field.name_ua = u'область гуманітарних наук'
            field.name_ru = u'область гуманитарных наук '
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='archeology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'archeology'
            discipline.name_pl = u'archeologia'
            discipline.name_en = u'archeology'
            discipline.name_ua = u'археологія'
            discipline.name_ru = u'археология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='art studies')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'art studies'
            discipline.name_pl = u'nauki o sztuce'
            discipline.name_en = u'art studies'
            discipline.name_ua = u'наука про мистецтво'
            discipline.name_ru = u'искусствоведение'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='bibliology and informatology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'bibliology and informatology'
            discipline.name_pl = u'bibliologia i informatologia'
            discipline.name_en = u'bibliology and informatology'
            discipline.name_ua = u'бібліологія і інформатологія'
            discipline.name_ru = u'библиология и информатология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='cultural')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'cultural'
            discipline.name_pl = u'kulturoznawstwo'
            discipline.name_en = u'cultural'
            discipline.name_ua = u'культурознавство'
            discipline.name_ru = u'культуроведение'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='ethnology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'ethnology'
            discipline.name_pl = u'etnologia'
            discipline.name_en = u'ethnology'
            discipline.name_ua = u'етнологія'
            discipline.name_ru = u'этнология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='family studies')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'family studies'
            discipline.name_pl = u'nauki o rodzinie'
            discipline.name_en = u'family studies'
            discipline.name_ua = u'наука про родину'
            discipline.name_ru = u'наука о семье'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='history')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'history'
            discipline.name_pl = u'historia'
            discipline.name_en = u'history'
            discipline.name_ua = u'історія'
            discipline.name_ru = u'история'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='history of art')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'history of art'
            discipline.name_pl = u'historia sztuki'
            discipline.name_en = u'history of art'
            discipline.name_ua = u'історія мистецтв'
            discipline.name_ru = u'история искусств'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='linguistics')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'linguistics'
            discipline.name_pl = u'językoznawstwo'
            discipline.name_en = u'linguistics'
            discipline.name_ua = u'лінгвістика'
            discipline.name_ru = u'лингвистика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='literature')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'literature'
            discipline.name_pl = u'literaturoznawstwo'
            discipline.name_en = u'literature'
            discipline.name_ua = u'література'
            discipline.name_ru = u'литература'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='management science')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'management science'
            discipline.name_pl = u'nauki o zarządzaniu'
            discipline.name_en = u'management science'
            discipline.name_ua = u'менеджмент'
            discipline.name_ru = u'менеджмент'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='philosophy')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'philosophy'
            discipline.name_pl = u'filozofia'
            discipline.name_en = u'philosophy'
            discipline.name_ua = u'філософія'
            discipline.name_ru = u'философия'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='religious studies')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'religious studies'
            discipline.name_pl = u'religioznawstwo'
            discipline.name_en = u'religious studies'
            discipline.name_ua = u'релігієзнавство'
            discipline.name_ru = u'религиоведение'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.KnowledgeArea'].objects.filter(name_en='area of the social sciences')) == 0:
            area = orm['trinity.KnowledgeArea']()
            area.name = u'area of the social sciences'
            area.name_pl = u'obszar nauk społecznych'
            area.name_en = u'area of the social sciences'
            area.name_ua = u'область суспільних наук'
            area.name_ru = u'область общественных наук'
            area.save()

        if len(orm['trinity.EducationField'].objects.filter(name_en='legal')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'legal'
            field.name_pl = u'dziedzina nauk prawnych'
            field.name_en = u'legal'
            field.name_ua = u'область юридичних наук'
            field.name_ru = u'область юридических наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='canonical law')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'canonical law'
            discipline.name_pl = u'prawo kanoniczne'
            discipline.name_en = u'canonical law'
            discipline.name_ua = u'канонічне право'
            discipline.name_ru = u'каноническое право'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='law')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'law'
            discipline.name_pl = u'prawo'
            discipline.name_en = u'law'
            discipline.name_ua = u'юриспруденція'
            discipline.name_ru = u'юриспруденция'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='science of administration')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'science of administration'
            discipline.name_pl = u'nauki o administracji'
            discipline.name_en = u'science of administration'
            discipline.name_ua = u'наука про управління'
            discipline.name_ru = u'наука об управлении'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='economical')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'economical'
            field.name_pl = u'dziedzina nauk ekonomicznych'
            field.name_en = u'economical'
            field.name_ua = u'область економічних наук'
            field.name_ru = u'область экономических наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='commodities')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'commodities'
            discipline.name_pl = u'towaroznawstwo'
            discipline.name_en = u'commodities'
            discipline.name_ua = u'товарознавство'
            discipline.name_ru = u'товароведение'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='economy')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'economy'
            discipline.name_pl = u'ekonomia'
            discipline.name_en = u'economy'
            discipline.name_ua = u'економіка'
            discipline.name_ru = u'экономика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='finance')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'finance'
            discipline.name_pl = u'finanse'
            discipline.name_en = u'finance'
            discipline.name_ua = u'фінанси'
            discipline.name_ru = u'финансы'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='management science')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'management science'
            discipline.name_pl = u'nauki o zarządzaniu'
            discipline.name_en = u'management science'
            discipline.name_ua = u'менеджмент'
            discipline.name_ru = u'менеджмент'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationField'].objects.filter(name_en='social')) == 0:
            field = orm['trinity.EducationField']()
            field.area = area
            field.name = u'social'
            field.name_pl = u'dziedzina nauk społecznych'
            field.name_en = u'social'
            field.name_ua = u'область суспільних наук'
            field.name_ru = u'область общественных наук'
            field.save()

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='media studies')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'media studies'
            discipline.name_pl = u'nauki o mediach'
            discipline.name_en = u'media studies'
            discipline.name_ua = u'наука про масову комунікацію'
            discipline.name_ru = u'массовая коммуникация'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='pedagogy')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'pedagogy'
            discipline.name_pl = u'pedagogika'
            discipline.name_en = u'pedagogy'
            discipline.name_ua = u'педагогіка'
            discipline.name_ru = u'педагогика'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='political science')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'political science'
            discipline.name_pl = u'nauki o polityce'
            discipline.name_en = u'political science'
            discipline.name_ua = u'політична наука'
            discipline.name_ru = u'политическая наука'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='psychology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'psychology'
            discipline.name_pl = u'psychologia'
            discipline.name_en = u'psychology'
            discipline.name_ua = u'психологія'
            discipline.name_ru = u'психология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='safety science')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'safety science'
            discipline.name_pl = u'nauki o bezpieczeństwie'
            discipline.name_en = u'safety science'
            discipline.name_ua = u'наука про безпеку'
            discipline.name_ru = u'наука о безопасности'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='science of cognition and social communication')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'science of cognition and social communication'
            discipline.name_pl = u'nauki o poznaniu i komunikacji społecznej'
            discipline.name_en = u'science of cognition and social communication'
            discipline.name_ua = u'наука про пізання і соціальну комунікацію'
            discipline.name_ru = u'наука о познании и социальной коммуникации'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='science of public policy')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'science of public policy'
            discipline.name_pl = u'nauki o polityce publicznej'
            discipline.name_en = u'science of public policy'
            discipline.name_ua = u'наука про громадську політику'
            discipline.name_ru = u'наука об общественной политике'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='sociology')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'sociology'
            discipline.name_pl = u'socjologia'
            discipline.name_en = u'sociology'
            discipline.name_ua = u'соціологія'
            discipline.name_ru = u'социология'
            discipline.save()
            field.discipline.add(discipline)

        if len(orm['trinity.EducationDiscipline'].objects.filter(name_en='the science of defense')) == 0:
            discipline = orm['trinity.EducationDiscipline']()
            discipline.name = u'the science of defense'
            discipline.name_pl = u'nauki o obronności'
            discipline.name_en = u'the science of defense'
            discipline.name_ua = u'наука про оборону'
            discipline.name_ru = u'наука о защите'
            discipline.save()
            field.discipline.add(discipline)


    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'merovingian.course': {
            'Meta': {'ordering': "('-is_active', 'name', '-level__name', '-type__name', 'profile__name', 'start_date')", 'object_name': 'Course', 'db_table': "'merv_course'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'end_date'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_first': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_first'", 'blank': 'True'}),
            'is_last': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_last'", 'blank': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']", 'db_column': "'id_level'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']", 'null': 'True', 'db_column': "'id_profile'", 'blank': 'True'}),
            'semesters': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'semesters'", 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'start_date'", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseType']", 'db_column': "'id_type'"}),
            'years': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'years'", 'blank': 'True'})
        },
        'merovingian.courselevel': {
            'Meta': {'object_name': 'CourseLevel', 'db_table': "'merv_course_level'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.courseprofile': {
            'Meta': {'object_name': 'CourseProfile', 'db_table': "'merv_course_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.coursetype': {
            'Meta': {'object_name': 'CourseType', 'db_table': "'merv_course_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.didacticoffer': {
            'Meta': {'object_name': 'DidacticOffer', 'db_table': "'merv_didactic_offer'"},
            'end_date': ('django.db.models.fields.DateField', [], {'db_column': "'end_date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_column': "'start_date'"})
        },
        'merovingian.module': {
            'Meta': {'ordering': "['name']", 'object_name': 'Module', 'db_table': "'merv_module'"},
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.ModuleType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduletype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ModuleType', 'db_table': "'merv_module_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        },
        'trinity.arealearningoutcome': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'AreaLearningOutcome', 'db_table': "'trinity_alo'"},
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_area': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationArea']"}),
            'education_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationCategory']"}),
            'education_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']"}),
            'education_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ru': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ua': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.courselearningoutcome': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'CourseLearningOutcome', 'db_table': "'trinity_clo'"},
            'alos': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trinity.AreaLearningOutcome']", 'db_table': "'trinity_clo__to__alo'", 'symmetrical': 'False'}),
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ru': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ua': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationarea': {
            'Meta': {'ordering': "['name']", 'object_name': 'EducationArea', 'db_table': "'trinity_education_area'"},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_education_area__to__course'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationcategory': {
            'Meta': {'ordering': "['-name']", 'object_name': 'EducationCategory', 'db_table': "'trinity_education_category'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationdiscipline': {
            'Meta': {'ordering': "['name']", 'object_name': 'EducationDiscipline', 'db_table': "'trinity_education_discipline'"},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_education_discipline__to__course'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationfield': {
            'Meta': {'ordering': "['area']", 'object_name': 'EducationField', 'db_table': "'trinity_education_field'"},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.KnowledgeArea']"}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_education_field__to__course'", 'blank': 'True'}),
            'discipline': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trinity.EducationDiscipline']", 'null': 'True', 'db_table': "'trinity_education_field__to__education_discipline'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.knowledgearea': {
            'Meta': {'ordering': "['name']", 'object_name': 'KnowledgeArea', 'db_table': "'trinity_knowledge_area'"},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_knowledge_area__to__course'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.modulelearningoutcome': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'ModuleLearningOutcome', 'db_table': "'trinity_mlo'"},
            'clos': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trinity.CourseLearningOutcome']", 'db_table': "'trinity_mlo__to__clo'", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']"}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ru': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_ua': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.trinityprofile': {
            'Meta': {'object_name': 'TrinityProfile', 'db_table': "'trinity_profile'"},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_profile__to__course'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True'})
        }
    }

    complete_apps = ['trinity']
    symmetrical = True
