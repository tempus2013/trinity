# -*- coding: utf-8 -*-

"""
Created on 24-01-2012

@author: pwierzgala
"""

from django import forms
from django.utils.encoding import force_unicode
from itertools import chain
from django.forms.widgets import CheckboxInput
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __

from apps.merovingian.models import Module, SGroup
from apps.trinity.models import EducationCategory

#---------------------------------------------------
#--- FORMULARZE
#---------------------------------------------------


class ModuleSelect(forms.Form):
    """
    Module select form.
    """ 
    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop('queryset', None)
        super(ModuleSelect, self).__init__(*args, **kwargs)
        if self.queryset is not None:
            self.fields['module'] = forms.ModelChoiceField(queryset=self.queryset, label=_(u'Module'), required=True)
        else:
            self.fields['module'] = forms.ModelChoiceField(queryset=Module.objects.none(), label=_(u'Module'), required=True)


class SGroupSelect(forms.Form):
    """
    Speciality select form.
    """ 
    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop('queryset', None)
        super(SGroupSelect, self).__init__(*args, **kwargs)
        if self.queryset is not None:
            self.fields['sgroup'] = SGroupModelChoiceField(queryset=self.queryset, label=_(u'Speciality'), required=True)
        else:
            self.fields['sgroup'] = SGroupModelChoiceField(queryset=SGroup.objects.none(), label=_(u'Speciality'), required=True)


class EducationAreaForm(forms.Form):
    """
    Education area multiple choice form.
    """
    def __init__(self, *args, **kwargs):
        education_areas = kwargs.pop('education_areas', [])
        widget = forms.CheckboxSelectMultiple(choices=education_areas)
        super(EducationAreaForm, self).__init__(*args, **kwargs)
        self.fields['education_areas'] = forms.MultipleChoiceField(choices=education_areas, widget=widget, required=False)


class KnowledgeAreaForm(forms.Form):
    """
    Knowledge area multiple choice form.
    """
    def __init__(self, *args, **kwargs):
        knowledge_areas = kwargs.pop('knowledge_areas', [])
        widget = forms.CheckboxSelectMultiple()
        super(KnowledgeAreaForm, self).__init__(*args, **kwargs)
        self.fields['knowledge_areas'] = forms.MultipleChoiceField(choices=knowledge_areas, widget=widget, required=False)


class EducationFieldForm(forms.Form):
    """
    Education field multiple choice form.
    """
    def __init__(self, *args, **kwargs):
        education_fields = kwargs.pop('education_fields', [])
        widget = forms.CheckboxSelectMultiple()
        super(EducationFieldForm, self).__init__(*args, **kwargs)
        self.fields['education_fields'] = forms.MultipleChoiceField(choices=education_fields, widget=widget, required=False)


class EducationDisciplineForm(forms.Form):
    """
    Education discipline multiple choice form.
    """
    def __init__(self, *args, **kwargs):
        education_disciplines = kwargs.pop('education_disciplines', [])
        widget = forms.CheckboxSelectMultiple()
        super(EducationDisciplineForm, self).__init__(*args, **kwargs)
        self.fields['education_disciplines'] = forms.MultipleChoiceField(choices=education_disciplines, widget=widget, required=False)


class EducationCategoryForm(forms.Form):
    """
    Education category multiple choice form.
    """
    education_category = forms.ModelChoiceField(queryset=EducationCategory.objects.all(), label=_(u'Education category'), required=True)


class MEEAddForm(forms.Form):
    """
    Course learning outcome adding/editing form.
    """
    def __init__(self, *args, **kwargs):
        learning_outcomes = kwargs.pop('learning_outcomes', [])
        widget = LearningOutcomesCheckboxSelectMultipleWidget(choices=learning_outcomes)
        super(MEEAddForm, self).__init__(*args, **kwargs)
        self.fields['learning_outcomes'] = forms.MultipleChoiceField(choices=learning_outcomes, widget=widget, required=False)
    
    symbol = forms.CharField(required=True)
    description = forms.CharField(widget=forms.Textarea, required=True)


class XEEAddForm(forms.Form):
    """
    Module learning outcome adding/editing form.
    """
    def __init__(self, *args, **kwargs):
        learning_outcomes = kwargs.pop('learning_outcomes', [])
        widget = LearningOutcomesCheckboxSelectMultipleWidget(choices=learning_outcomes)
        super(XEEAddForm, self).__init__(*args, **kwargs)
        self.fields['learning_outcomes'] = forms.MultipleChoiceField(choices=learning_outcomes, widget=widget, required=False)
        
    symbol = forms.CharField(required=True)
    description = forms.CharField(widget=forms.Textarea, required=True)

#---------------------------------------------------
#--- FIELDS
#--------------------------------------------------- 


class SGroupModelChoiceField(forms.ModelChoiceField):
    """
    Speciality choice field.
    """
    def label_from_instance(self, obj):
        return obj.name  

#---------------------------------------------------
#--- WIDGETS
#--------------------------------------------------- 


class LearningOutcomesCheckboxSelectMultipleWidget(forms.widgets.CheckboxSelectMultiple):
    """
    Multiple choice list widget with learning outcomes. It displays table with columns for: choice field, symbol and description.
    """
    def __init__(self, *args, **kwargs):
        self.choices = kwargs.pop('choices', None)
        super(LearningOutcomesCheckboxSelectMultipleWidget, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None, choices=()):
        if value is None:
            value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        rows = []
        
        # Normalize to strings
        str_values = set([force_unicode(v) for v in value])
        for i, (id, learning_outcome) in enumerate(chain(self.choices, choices)):
            
            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                label_for = u' for="%s"' % final_attrs['id']
            else:
                label_for = ''

            # Przygotowanie elementów wiersza
            cb = CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
            id = force_unicode(learning_outcome.id)
            rendered_cb = cb.render(name, id)
            symbol = conditional_escape(force_unicode(learning_outcome.symbol))
            description = conditional_escape(force_unicode(learning_outcome.description))
            
            # Wygenerowanie wiersza
            row = '<tr>'
            row += '<td class="input">%s</td>' % rendered_cb
            row += '<td><label %s>%s</label></td>' % (label_for, symbol)
            row += '<td><label %s>%s</label></td>' % (label_for, description)
            row += '</tr>'
            
            # Dodanie wiersza do listy wierszy
            rows.append(row)
        
        # Wygenerowanie tabeli
        output = u''
        if rows:
            output += '<table class="table_0">'
            output += '<thead>'
            output += '<tr>'
            output += '<td class="corner"></td>'
            output += '<td class="answer">%s</td>' % __(u'Symbol')
            output += '<td class="answer">%s</td>' % __(u'Description')
            output += '</tr>'
            output += '</thead>'
            output += '<tbody>'
            output += '%s' % u'\n'.join(rows)
            output += '</tbody>'
            output += '</table>'
        else:
            output += '<ul class="infolist"><li>%s</li></ul>' % __(u'List of learning outcomes is empty.')
        return mark_safe(output)      