# -*- coding: utf-8 -*-

from django.db import models
from apps.syjon.lib.validators import validate_white_space
from django.utils.translation import ugettext_lazy as _

from apps.merovingian.models import Course, Module, CourseProfile
from apps.trainman.models import UserProfile


class TrinityProfile(models.Model):
    """
    Administrator efektów kształcenia.
    """
    class Meta:
        db_table = 'trinity_profile'
        verbose_name = _(u'Administrator')
        verbose_name_plural = _(u'Administrators')

    user_profile = models.OneToOneField(UserProfile, verbose_name=_(u'User'))
    courses = models.ManyToManyField(Course, db_table='trinity_profile__to__course', null=True, blank=True)
    
    def __unicode__(self):
        return self.user_profile.user.username
    
    @staticmethod
    def get_courses(user):
        if user.is_superuser:
            return Course.objects.active()
        else:
            return TrinityProfile.objects.get(user_profile=user.get_profile()).courses.filter(is_active=True)


class EducationArea(models.Model):
    """
    Obszar kształcenia.
    """
    class Meta:
        db_table = 'trinity_education_area'
        verbose_name = _(u'Area of education')
        verbose_name_plural = _(u'Areas of education')
        ordering = ['name']
        
    name = models.CharField(max_length=255)
    courses = models.ManyToManyField(Course, null=True, blank=True, db_table='trinity_education_area__to__course')
    
    def __unicode__(self):
        return self.name


class EducationCategory(models.Model):
    """
    Kategoria kształcenia.
    """
    class Meta:
        db_table = 'trinity_education_category'
        verbose_name = _(u'Education category')
        verbose_name_plural = _(u'Education categories')
        ordering = ['-name']
        
    name = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.name
    
#---------------------------------------------------
#--- KATEGORYZACJA KIERUNKÓW III STOPNIA
#---------------------------------------------------


class KnowledgeArea(models.Model):
    """
    Obszar wiedzy.
    """
    class Meta:
        db_table = 'trinity_knowledge_area'
        verbose_name = _(u'Area of knowledge')
        verbose_name_plural = _(u'Areas of knowledge')
        ordering = ['name']
        
    name = models.CharField(max_length=255)
    courses = models.ManyToManyField(Course, null=True, blank=True, db_table='trinity_knowledge_area__to__course')
    
    def __unicode__(self):
        return self.name   


class EducationField(models.Model):
    """
    Obszar nauki.
    """
    class Meta:
        db_table = 'trinity_education_field'
        verbose_name = _(u'Field of science')
        verbose_name_plural = _(u'Fields of science')
        ordering = ['area']
        
    name = models.CharField(max_length=255)
    area = models.ForeignKey('KnowledgeArea')
    discipline = models.ManyToManyField('EducationDiscipline', null=True, blank=True, db_table='trinity_education_field__to__education_discipline')
    courses = models.ManyToManyField(Course, null=True, blank=True, db_table='trinity_education_field__to__course')
    
    def __unicode__(self):
        return self.name


class EducationDiscipline(models.Model):
    """
    Dyscyplina nauki.
    """
    class Meta:
        db_table = 'trinity_education_discipline'
        verbose_name = _(u'Discipline of science')
        verbose_name_plural = _(u'Disciplines of science')
        ordering = ['name']
        
    name = models.CharField(max_length=255)
    courses = models.ManyToManyField(Course, null=True, blank=True, db_table='trinity_education_discipline__to__course')
    
    def __unicode__(self):
        return self.name

#---------------------------------------------------
#--- EFEKTY KSZTAŁCENIA
#--------------------------------------------------- 


class AreaLearningOutcome(models.Model):
    """
    Obszarowy efekt kształcenia.
    """
    class Meta:
        db_table = 'trinity_alo'
        verbose_name = _(u'Area learning outcome')
        verbose_name_plural = _(u'Area learning outcomes')
        ordering = ['symbol']

    symbol = models.CharField(max_length=16, validators=[validate_white_space])
    description = models.TextField(validators=[validate_white_space])
    education_area = models.ForeignKey('EducationArea')
    education_level = models.ForeignKey('merovingian.CourseLevel')
    education_profile = models.ForeignKey(CourseProfile)
    education_category = models.ForeignKey('EducationCategory')

    def __unicode__(self):
        return self.symbol


class CourseLearningOutcome(models.Model):
    """
    Kierunkowy efekt kształcenia.
    """
    class Meta:
        db_table = 'trinity_clo'
        verbose_name = _(u'Course learning outcome')
        verbose_name_plural = _(u'Course learning outcomes')
        ordering = ['symbol']

    course = models.ForeignKey(Course)
    education_category = models.ForeignKey('EducationCategory')
    symbol = models.CharField(max_length=16, validators=[validate_white_space])
    description = models.TextField(validators=[validate_white_space])
    alos = models.ManyToManyField('AreaLearningOutcome', db_table='trinity_clo__to__alo')
    
    def __unicode__(self):
        return self.symbol
    
    def get_related_modules(self):
        """
        Zwraca zbiór modułów powiązanych z kierunkowym efektem kształcenia.
        """
        return Module.objects.filter(modulelearningoutcome__clos=self).distinct()


class ModuleLearningOutcome(models.Model):
    """
    Modułowy efekt kształcenia.
    """
    class Meta:
        db_table = 'trinity_mlo'
        verbose_name = _(u'Module learning outcome')
        verbose_name_plural = _(u'Module learning outcomes')
        ordering = ['symbol']
    
    module = models.ForeignKey(Module)
    symbol = models.CharField(max_length=16, validators=[validate_white_space])
    description = models.TextField(validators=[validate_white_space])
    clos = models.ManyToManyField('CourseLearningOutcome', db_table='trinity_mlo__to__clo')
    
    def get_related_alos(self):
        """
        Zwraca zbiór obszarowych efketów kształcenia powiązanych z modułowym efektem kształcenia. Obszarowe efekty kształcenia powiązane są z modułowymi efektami kształcenia pośrednio przez kierunkowe efekty kształcenia.
        """
        return AreaLearningOutcome.objects.filter(courselearningoutcome__modulelearningoutcome=self).distinct()

    def __unicode__(self):
        return self.symbol