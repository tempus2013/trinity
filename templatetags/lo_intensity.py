# -*- coding: utf-8 -*-

from django import template
from apps.trinity.models import ModuleLearningOutcome

register = template.Library()


@register.inclusion_tag('trinity/templatetags/lo_intensity.html')
def get_intensity(clo, module):
    intensity = ModuleLearningOutcome.objects.filter(module=module, clos=clo).count()
    return {'intensity': intensity}